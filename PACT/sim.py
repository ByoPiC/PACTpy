#! /usr/bin/python
import numpy as np
import healpy as hp
import os 
from pixell import enmap, enplot, reproject, utils, curvedsky
from astropy.io import fits

def milca_weights(cov,FF):

    invC = np.linalg.pinv(cov)
    FinvC = FF.dot(invC)
    den = FinvC.dot(FF.T)
    ww = (np.linalg.pinv(den)).dot(FinvC)

    return ww


fout = 0
figdir = '../../figs/Sims/planck/'
indir = '../../data/'

Nside = 2048; Npix = hp.nside2npix(Nside)
#Nside = 4096; Npix = hp.nside2npix(Nside)
lmax = 5800; lmax_arrsize = hp.Alm.getsize(lmax-1)
lpix = hp.pixwin(Nside, lmax=lmax-1)
ybeam = hp.gauss_beam(np.deg2rad(2.4/60.), lmax=lmax-1)

websky_path = '/data/cluster/byopic/victor/WebSky/data/'
mysim_path = '../data/healpix/'

freq = np.array([100, 143, 217, 353, 545, 98, 150])
CMB_resp = np.array([1,1,1,1,1,1,1]) 
SZ_resp = np.array([-4.111550, -2.836906, -0.022856, 6.105222, 15.2554374, -4.162224, -2.599713])
Nmap = len(freq)

ytrue = hp.read_map(mysim_path + 'websky_tsz_nside%d.fits' %(Nside))
alm_ytrue = hp.map2alm(ytrue, lmax=lmax-1)
cl_ytrue = hp.alm2cl(alm_ytrue, lmax=lmax-1)
ell = np.arange(len(cl_ytrue))

alm_ytrue_beam = hp.almxfl(alm_ytrue, ybeam)
cl_ytrue_beam = hp.alm2cl(alm_ytrue_beam, lmax=lmax-1)
#shape,wcs = tsz.shape, tsz.wcs

Nfil = 16
f1 = fits.open(indir+'filters/filters_5800_%d_log.fits' %(Nfil))  # open a FITS file
tbdata1 = f1[1].data  # assume the first extension is a table
fl = np.zeros((Nfil,lmax))
for I in range(Nfil):
    fl[I] = tbdata1.field('col%d' %(I))[:-1]

alm_ytrue_fl = np.zeros((Nfil, lmax_arrsize), dtype=np.complex128)
cl_ytrue_fl = np.zeros((Nfil, lmax))

alm_ytrue_beam_fl = np.zeros((Nfil, lmax_arrsize), dtype=np.complex128)
cl_ytrue_beam_fl = np.zeros((Nfil, lmax))


for J in range(Nfil): # fl_i

    print('%dth filter out of %d' %(J, Nfil-1))
    alm_ytrue_fl[J] = hp.almxfl(alm_ytrue, fl[J])
    cl_ytrue_fl[J] = hp.alm2cl(alm_ytrue_fl[J], lmax=lmax-1)

    alm_ytrue_beam_fl[J] = hp.almxfl(alm_ytrue_beam, fl[J])
    cl_ytrue_beam_fl[J] = hp.alm2cl(alm_ytrue_beam_fl[J], lmax=lmax-1)

alm_ytrue_beam_fl_sum = np.sum(alm_ytrue_beam_fl,axis=0)
cl_ytrue_beam_fl_sum = hp.alm2cl(alm_ytrue_beam_fl_sum, lmax=lmax-1)


########################################
## Data simulation
########################################

bls = np.zeros((Nmap, lmax))
beams = [9.68, 7.30, 5.02, 4.94, 4.83, 2.2, 1.4]
for I in range(Nmap): # map
    
    bls[I] = hp.gauss_beam(np.deg2rad(beams[I]/60.), lmax=lmax-1)        

alm_arr = np.zeros((Nmap, lmax_arrsize), dtype=np.complex128)

# Make simulated maps
for I in range(Nmap):

    print('Simulating %dth map out of %d' %(I+1, Nmap))

    alm_arr[I] = hp.read_alm(mysim_path + 'alm_websky_%d_tsz_cmb_no_noise_with_beam_nside2048.fits' %(freq[I])) * 1e-6

alm_arr_fl = np.zeros((Nmap, Nfil, lmax_arrsize), dtype=np.complex128)
map_arr_fl = np.zeros((Nmap, Nfil, Npix))

# Preprocessing data (same beam, filter)
for I in range(Nmap):

    print('Preprocessing %dth map out of %d' %(I+1, Nmap))
    alm_arr_i = hp.almxfl(alm_arr[I], ybeam/bls[I])

    for J in range(Nfil):        
        alm_arr_fl[I][J] = hp.almxfl(alm_arr_i, fl[J])
        map_arr_fl[I][J] = hp.alm2map(alm_arr_fl[I][J], Nside, lmax=lmax-1)


resp = np.zeros((Nfil,Nmap))
for J in range(Nfil): # fl_i
    for I in range(Nmap): # map

        resp[J][I] = 1.


alm1_out_fl = np.zeros((Nfil,lmax_arrsize), dtype=np.complex128)
alm2_out_fl = np.zeros((Nfil,lmax_arrsize), dtype=np.complex128)
ymaps2 = np.zeros((Nfil,Npix))

Nact = 3; 
imin_act = np.int32(Nfil - Nact)

for J in range(Nfil): # fl_i    

    print('%dth filter out of %d' %(J, Nfil-1))
    
    if(J >= imin_act):
        use_flag_j = (np.array([0,0,1,1,1, 1,1])==1)

    else:
        use_flag_j = (np.array([1,1,1,1,1, 0,0])==1)
    
    Nmap_eff = np.sum(use_flag_j)
    print("freq: ", freq[use_flag_j]) 

    cov_map = np.cov(map_arr_fl[use_flag_j,J], bias=True)
    covm_map = np.matrix(cov_map)

    CMB = np.zeros(Nmap_eff)
    CMB = CMB_resp[use_flag_j] * resp[J][use_flag_j]
    CMB = np.append(CMB,0)
    
    SZ = np.zeros(Nmap_eff)
    SZ = SZ_resp[use_flag_j] * resp[J][use_flag_j]    
    SZ = np.append(SZ, 0)

    Mat1 = np.insert(covm_map, Nmap_eff, -1, axis=1)
    Mat2 = np.insert(Mat1, Nmap_eff, CMB, axis=0)
    Mat = np.matrix(np.insert(Mat2, Nmap_eff+1, SZ, axis=0))
    V = np.zeros(Nmap_eff+2)
    V[-1] = 1

    coef_ILC = np.dot(Mat.I, V)
    coef_j = np.array(coef_ILC)[0][:-1]
    print("coef: ", coef_j)

    FF = np.array([SZ[:-1]*resp[J][use_flag_j], CMB[:-1]*resp[J][use_flag_j]])
    ww2 = milca_weights(covm_map,FF)
    coef2 = np.array(ww2)[0]
    print("coef2: ", coef2)

    alm1_out_fl[J] = np.sum(alm_arr_fl[use_flag_j,J] * coef_j.reshape(Nmap_eff,1), axis=0)    
    alm2_out_fl[J] = np.sum(alm_arr_fl[use_flag_j,J] * coef2.reshape(Nmap_eff,1), axis=0)
    ymaps2[J] = np.sum(map_arr_fl[use_flag_j,J] * coef2.reshape(Nmap_eff,1), axis=0) 


cl1_smth = hp.alm2cl(np.sum(alm1_out_fl,axis=0))    
cl2_smth = hp.alm2cl(np.sum(alm2_out_fl,axis=0))
cl1_smth_cross = hp.alm2cl(np.sum(alm1_out_fl,axis=0), alm_ytrue_beam, lmax=lmax-1)
cl2_smth_cross = hp.alm2cl(np.sum(alm2_out_fl,axis=0), alm_ytrue_beam, lmax=lmax-1)


fact = ell*(ell+1)/2./np.pi
plt.figure()
plt.plot(ell, fact*cl_ytrue_beam, 'k', label="tSZ (True)")
#plt.plot(ell, fact*cl1_smth, 'b', label="tSZ (My)")
plt.plot(ell, fact*cl2_smth, 'r', label="tSZ (My)")
plt.plot(ell, fact*cl2_smth_cross, 'b', label="tSZ cross")

plt.xscale('log')
plt.yscale('log')
plt.legend(loc='upper left')
plt.ylim(1e-16, 1e-11)
plt.xlabel('$\\ell$')
plt.ylabel('$D_{\\ell}$')
plt.tight_layout()

if (fout == 1):
    plt.savefig(figdir+'Dlyy_hp_tsz_cmb_cib_with_beam_no_noise.png')

plt.figure()
plt.plot(ell, cl2_smth/cl_ytrue_beam, 'r')
plt.plot(ell, cl2_smth_cross/cl_ytrue_beam, 'b')
#plt.plot(ell, cl1_smth_cross/cl_ytrue_beam, 'm')
ylim(0.,2.0)
plt.xlabel('$\\ell$')
plt.ylabel('$D_{\\ell,out}/D_{\\ell,true}$')

if (fout == 1):
    plt.savefig(figdir+'Dlyy_ratio_hp_tsz_cmb_cib_with_beam_no_noise.png')

