#! /usr/bin/python
import numpy as np
import healpy as hp
import os 
from pixell import enmap, enplot, reproject, utils, curvedsky
from astropy.io import fits
import nawrapper as nw

def milca_weights(cov,FF):

    invC = np.linalg.pinv(cov)
    FinvC = FF.dot(invC)
    den = FinvC.dot(FF.T)
    ww = (np.linalg.pinv(den)).dot(FinvC)
    return ww


fout = 0
figdir = '../../figs/data/'
os.system('mkdir -p %s' %(figdir))
indir = '../../data/'

lmax = 5800; lmax_arrsize = hp.Alm.getsize(lmax-1)
lpix = hp.pixwin(2048, lmax=lmax-1)

ybeam = hp.gauss_beam(np.deg2rad(2.4/60.), lmax=lmax-1)

### ACT BN Footprint
actpath = "/data/cluster/byopic/htanimur/data/ACT/DR4/"
footprint = enmap.read_map(actpath + 'masks/compsep_masks/act_dr4.01_s14s15_BN_compsep_mask.fits') 
shape,wcs = footprint.shape, footprint.wcs

psmask = enmap.extract(enmap.read_map(actpath + 'masks/source_masks/act_dr4.01_mask_s13s16_0.100mJy_5.0arcmin.fits'), shape, wcs)
psmask_apo = (nw.apod_C2(psmask, (10./60.)) > 0.954)

ymask4 = enmap.extract(enmap.read_map(indir+'planck4_psmask_5freq.fits'), shape, wcs)

mask = footprint * psmask * ymask4

freq = np.array([30, 44, 70, 100, 143, 217, 353, 545, 857, 98, 150])
CMB_resp = np.array([1,1,1,1,1,1,1,1,1,1,1])
SZ_resp = np.array([-5.33553334355726, -5.177444325526258, -4.753306928407555, -4.031088613564252, -2.785153126887146, 0.19245262678526642, 6.2058028396847496, 14.453860637904553, 26.328323100285488, -4.152680583504223, -2.6183240190543966])

Nmap = len(freq)

data_arr = enmap.zeros((Nmap, shape[0], shape[1]), wcs)
noise_arr = enmap.zeros((Nmap, shape[0], shape[1]), wcs)
signal_arr = enmap.zeros((Nmap, shape[0], shape[1]), wcs)
alm_arr = np.zeros((Nmap, lmax_arrsize), dtype=complex128)
alm_signal_arr = np.zeros((Nmap, lmax_arrsize), dtype=complex128)
for I in range(Nmap):

    if(freq[I]==98 or freq[I]==150):
        data_arr_i = enmap.read_map(indir+'act%d_coadd.fits' %(freq[I]))*1e-6
        
        data_arr[I] = data_arr_i #- np.mean(data_arr_i)
        noise_arr[I] = enmap.read_map(indir+'act%d_coadd_noise.fits' %(freq[I]))*1e-6
    else:
        data_arr_i = enmap.read_map(indir+'planck%d.fits' %(freq[I]))*1e-6
        data_arr[I] = data_arr_i #- np.mean(data_arr_i)
        noise_arr[I] = enmap.read_map(indir+'planck%d_noise.fits' %(freq[I]))*1e-6

    alm_arr[I] = np.cdouble(curvedsky.map2alm(data_arr[I], lmax=lmax-1))
    alm_signal_arr[I] = np.cdouble(curvedsky.map2alm(signal_arr[I], lmax=lmax-1))


Nfil = 16
f1 = fits.open('./filters_5800_%d_log.fits' %(Nfil))   # open a FITS file
tbdata1 = f1[1].data  # assume the first extension is a table
fl = np.zeros((Nfil,lmax))
for I in range(Nfil):
    fl[I] = tbdata1.field('col%d' %(I))[:-1]


bls = np.zeros((Nmap, lmax))
beams = np.array([32.408, 27.1, 13.315, 9.68, 7.30, 5.02, 4.94, 4.83, 4.64, 2.2, 1.4])
for I in range(Nmap): # map

    if(freq[I] == 98 or freq[I] == 150):
        bls[I] = np.loadtxt(indir + 'actbeam%d.fits' %(freq[I]))[:lmax]
    else:
        bls[I] = hp.gauss_beam(np.deg2rad(beams[I]/60.), lmax=lmax-1)

    alm_arr[I] = hp.almxfl(alm_arr[I], ybeam/bls[I])    

resp = np.zeros((Nfil,Nmap))
for J in range(Nfil): # fl_i
    for I in range(Nmap): # map
        resp[J][I] = 1.


almsum = np.zeros(lmax_arrsize, dtype=complex128)
ymaps = enmap.zeros((Nfil, shape[0], shape[1]), wcs)
almsum2 = np.zeros(lmax_arrsize, dtype=complex128)
ymaps2 = enmap.zeros((Nfil, shape[0], shape[1]), wcs)
ymaps3 = enmap.zeros((Nfil, shape[0], shape[1]), wcs)

ymaps_fl_i = enmap.zeros((Nmap, shape[0], shape[1]), wcs)
ymaps_fl_mask_i = np.zeros((Nmap, np.int32(np.sum(mask==1))))

Nact = 3; 
imin_act = np.int32(Nfil - Nact)


for J in range(Nfil): # fl_i
    
    use_flag_j = np.full(Nmap, False)
    
    print("Jth filter: %d out of %d " %(J, Nfil-1))

    alm_i = np.zeros((Nmap, lmax_arrsize), dtype=complex128)
    alm_signal_i = np.zeros((Nmap, lmax_arrsize), dtype=complex128)    
    for I in range(Nmap):
        alm_arr_i = alm_arr[I]
        alm_i[I] = hp.almxfl(alm_arr_i, fl[J])
        curvedsky.alm2map(alm_i[I], ymaps_fl_i[I])
        ymaps_fl_mask_i[I] = ymaps_fl_i[I][mask==1]
        
    use_flag_j = (np.array([0,0,0, 1,1,1,1,1,0, 0,0])==1)

    if (J >= imin_act):
        use_flag_j = (np.array([0,0,0, 0,0,1,1,1,0, 1,1])==1)
    
    Nmap_eff = np.sum(use_flag_j)
    print("freq: ", freq[use_flag_j])        
    
    CMB = np.zeros(Nmap_eff)
    CMB = CMB_resp[use_flag_j] * resp[J][use_flag_j]
    CMB = np.append(CMB,0)
    
    SZ = np.zeros(Nmap_eff)
    SZ = SZ_resp[use_flag_j] * resp[J][use_flag_j]    
    SZ = np.append(SZ, 0)
    
    cov_j = np.cov(alm_i[use_flag_j], bias=True)
    cov_j_map = np.cov(ymaps_fl_mask_i[use_flag_j], bias=True)
    
    Mat1 = np.insert(cov_j, Nmap_eff, -1, axis=1)
    Mat2 = np.insert(Mat1, Nmap_eff, CMB, axis=0)
    Mat = np.matrix(np.insert(Mat2, Nmap_eff+1, SZ, axis=0))
    V = np.zeros(Nmap_eff+2)
    V[-1] = 1

    coef_ILC = np.dot(Mat.I, V)
    coef_j = np.array(coef_ILC)[0][:-1]
    print("coef: ", np.real(coef_j))

    covm = np.matrix(cov_j)
    FF = np.array([SZ[:-1], CMB[:-1]])
    ww2 = milca_weights(covm,FF)
    coef2 = np.array(ww2)[0]
    print("coef2: ", np.real(coef2))

    covm_map = np.matrix(cov_j_map)
    ww3 = milca_weights(covm_map,FF)
    coef3 = np.array(ww3)[0]
    print("coef3: ", np.real(coef3))    

    almsum_i = np.sum(alm_i[use_flag_j] * coef_j.reshape(Nmap_eff,1), axis=0)
    almsum = almsum + almsum_i
    curvedsky.alm2map(almsum_i, ymaps[J])
    
    almsum2_i = np.sum(alm_i[use_flag_j] * coef2.reshape(Nmap_eff,1), axis=0) 
    almsum2 = almsum2 + almsum2_i
    curvedsky.alm2map(almsum2_i, ymaps2[J])        

    ymaps3[J] = np.sum(ymaps_fl_i[use_flag_j] * coef3.reshape(Nmap_eff,1,1), axis=0) 

    
mask = footprint * psmask * ymask4

ymap_act = enmap.read_map(actpath + 'compsep_maps/tilec_single_tile_BN_comptony_deprojects_cmb_map_v1.2.0_joint.fits')
ymap_act_alm = np.cdouble(curvedsky.map2alm(ymap_act*mask, lmax=lmax-1))
cl = hp.alm2cl(ymap_act_alm)
ell = np.arange(len(cl))

ymap1_smth = np.sum(ymaps3[5:],axis=0)
ymap1_alm_smth = np.cdouble(curvedsky.map2alm(ymap1_smth*mask, lmax=lmax-1))
cl1_smth = hp.alm2cl(ymap1_alm_smth)
ymap1_smth = enmap.zeros((shape[0], shape[1]), wcs)
curvedsky.alm2map(ymap1_alm_smth, ymap1_smth)

ymap_pact_smth = ymap1_smth + ymap2_smth
ymap_pact_alm_smth = np.cdouble(curvedsky.map2alm(ymap_pact_smth*mask, lmax=lmax-1))
cl_ymap_pact_smth = hp.alm2cl(ymap_pact_alm_smth)

ymap_pact_smth = enmap.zeros((shape[0], shape[1]), wcs)
curvedsky.alm2map(ymap_pact_alm_smth, ymap_pact_smth)

fact = ell*(ell+1)/2./np.pi
plt.figure()
plt.plot(ell, fact*cl, 'k', label="tSZ (ACT)")
plt.plot(ell, fact*cl1_smth, 'm', label="tSZ (My ACT)")
#plt.plot(ell, fact*cl2_smth, 'b', label="tSZ (My Planck)")
plt.plot(ell, fact*cl_ymap_pact_smth, 'r', label="tSZ (PACT)")
plt.xscale('log')
plt.yscale('log')
plt.legend(loc='upper left')
plt.ylim(1e-16, 1e-9)
#plt.ylim(1e-15, 1e-13)
plt.xlabel('$\\ell$')
plt.ylabel('$D_{\\ell}$')
plt.tight_layout()

if(fout==1):
    plt.savefig(figdir+'Dlyy_logfilter%d_nact%d_nband%d_psmask.png' %(Nfil, Nact, Nmap_eff) )
   
xbins = np.linspace(-2e-4, 2e-4, 100)
plt.figure()
plt.hist(ymap_act[mask==1], bins=xbins, color='k', histtype='step')
plt.hist(ymap_pact_smth[mask==1], bins=xbins, color='r', histtype='step')
plt.xlabel('$Compton y$')
plt.ylabel('Num of pixels')
plt.yscale('log')
plt.tight_layout()

if(fout==1):
    plt.savefig(figdir+'yhist_logfilter%d_nact%d_nband%d.png' %(Nfil, Nact, Nmap_eff) )
    
if(fout==1):    
    keys = {"downgrade": 8, "ticks": 10, "colorbar": True, 'min': -5e-6, 'max': 5e-6}
    ymap_plot = enplot.get_plots(ymap_act*mask, **keys)
    enplot.write(figdir+'ymap_act_logfilter%d_nact%d_nband%d.png' %(Nfil, Nact, Nmap_eff), ymap_plot)

    ymap_pact_plot = enplot.get_plots(ymap_pact_smth*mask, **keys)
    enplot.write(figdir+'ymap_pact_logfilter%d_nact%d_nband%d.png' %(Nfil, Nact, Nmap_eff), ymap_pact_plot)    
    
    ymap1_plot = enplot.get_plots(ymap1_smth*mask, **keys)
    enplot.write(figdir+'ymap_out_act_logfilter%d_nact%d_nband%d.png' %(Nfil, Nact, Nmap_eff), ymap1_plot)

    ymap2_plot = enplot.get_plots(ymap2_smth*mask, **keys)
    enplot.write(figdir+'ymap_out_planck_logfilter%d_nact%d_nband%d.png' %(Nfil, Nact, Nmap_eff), ymap2_plot)

    
plt.figure()
plt.plot(ell, cl_ymap_pact_smth/cl, 'k')
plt.xscale('log')
ylim(0,2)
plt.xlabel('$\\ell$')
plt.ylabel('$D_{\\ell,output}/D_{\\ell,true}$')
plt.tight_layout()

if(fout==1):
    plt.savefig(figdir+'Dl_ratio_logfilter%d_nact%d_nband%d.png' %(Nfil, Nact, Nmap_eff))
